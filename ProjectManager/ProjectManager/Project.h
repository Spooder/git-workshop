#pragma once
#include <iostream>
class Project
{
public:
	Project (std::string m_name, std::string m_author);
	~Project(); //d'tor

	std::string getName() ;
	std::string getAuthor();

	void setName(std::string name);
	void setAuthor(std::string author);

	std::string toString();

private:
	std::string m_name;
	std::string m_author;
};