#pragma once
#include "ICommand.h"

class CommandModify : public ICommand
{
private:
	std::string m_projectName;
	std::string m_newName;
	std::string m_newAuthor;

public:
	CommandModify(const std::string& projectName, const std::string& newName, const std::string& newAuthor); // C'tor

	void doAction(std::vector<Project*>& projects) const;
};
