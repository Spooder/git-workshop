#include <iostream>
#include "Project.h"
#include "ICommand.h"
#include <vector>
#include <sstream>

void commandInvoker(const std::string& command, std::vector<Project>& projects);

void printMenu();

int main()
{
	std::string userInput = "";
	std::vector<Project> projects;

	do
	{
		printMenu();

		while (true)
		{
			try
			{
				std::getline(std::cin, userInput);
				// Break on success
				break;
			}
			catch (const std::exception & e)
			{
				std::cout << "Bad input\n";
			}
		}

		commandInvoker(userInput, projects);

	} while (userInput != "exit");


	return 0;
}

void commandInvoker(const std::string& command, std::vector<Project>& projects)
{
	ICommand* com = nullptr;
	std::vector<std::string> commandsVec;

	// Seperate by delimeter into vector
	std::istringstream iss(command);
	std::string item;
	while (std::getline(iss, item, ' '))
	{
		commandsVec.push_back(item);
	}

	if (commandsVec[0] == "new")
	{
		//new();
		std::cout << "new";
	}
	else if (commandsVec[0] == "show")
	{
		//show();
		std::cout << "show";
	}
	else if (commandsVec[0] == "delete")
	{
		std::cout << "delete";
		//delete();
	}
	else if (commandsVec[0] == "modify")
	{
		std::cout << "modify";
		//modify();
	}
}


void printMenu()
{
	std::cout << "Projects Manager - v0.0.1\n-------------------------\nnew <project - name> <author>\n" <<
		"show <project - name>\ndelete <project - name>\nmodify <project - name> <new - name>\n";
}
