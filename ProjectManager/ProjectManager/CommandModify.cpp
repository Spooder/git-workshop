#include "CommandModify.h"

// C'tor for command modify
CommandModify::CommandModify(const std::string& projectName, const std::string& newName, const std::string& newAuthor)
{
	this->m_projectName = projectName;
	this->m_newName = newName;
	this->m_newAuthor = newAuthor;
}


// Deletes the project with CommandDelete's field m_projectName as its name
void CommandModify::doAction(std::vector<Project*>& projects) const
{
	// Go over the vector
	for (std::vector<Project*>::iterator it = projects.begin(); it != projects.end(); it++)
	{
		// When matching project found, erase it and exit loop.
		if ((*it)->getName() == this->m_projectName)
		{
			// Modify project and break.
			(*it)->setName(this->m_newName);
			(*it)->setName(this->m_newAuthor);
			break;
		}
	}
}
