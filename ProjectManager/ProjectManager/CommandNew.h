#pragma once
#include <iostream>
#include "ICommand.h"

class CommandNew : public ICommand
{
private:
	CommandNew(const std::string m_name, const std::string m_author);
	void virtual doAction(std::vector<Project*>& projects) const;

public:
	std::string m_projectName;
	std::string m_projectAuthor;
};


