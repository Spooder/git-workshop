#include "CommandDelete.h"

// C'tor for CommandDelete
CommandDelete::CommandDelete(std::string& projectName)
{
	this->m_projectName = projectName;
}


// Deletes the project with CommandDelete's field m_projectName as its name
void CommandDelete::doAction(std::vector<Project*>& projects) const
{
	// Go over the vector
	for (std::vector<Project*>::iterator it = projects.begin(); it != projects.end(); it++)
	{
		// When matching project found, erase it and exit loop.
		if ((*it)->getName() == this->m_projectName)
		{
			Project* temp = (*it);
			projects.erase(it);
			delete temp;
			break;
		}
	}
}