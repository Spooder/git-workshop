#include "CommandNew.h"

CommandNew::CommandNew(const std::string m_name, const std::string m_author)
{
	this->m_projectName = m_name;
	this->m_projectAuthor = m_author;
}

void CommandNew::doAction(std::vector<Project*>& projects) const
{
	// Create a new project and add to the vector
	Project* newProject = new Project(this->m_projectName, this->m_projectAuthor);

	projects.push_back(newProject);
}