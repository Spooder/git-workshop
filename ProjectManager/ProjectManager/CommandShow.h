#pragma once

#include "ICommand.h"

class CommandShow : public ICommand
{
private:
	std::string m_projectName;

public:
	// C'tors
	CommandShow();
	CommandShow(const std::string& projectName);

	void virtual doAction(std::vector<Project>& projects) const;
	
};
