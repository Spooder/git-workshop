#include "CommandShow.h"

// C'tor for command show
CommandShow::CommandShow()
{
	this->m_projectName = "";
}

// C'tor for command show
CommandShow::CommandShow(const std::string& projectName)
{
	this->m_projectName = projectName;
}


void CommandShow::doAction(std::vector<Project>& projects) const
{
	for (Project p : projects)
	{
		// Print all project if m_projectName is empty, or specific project if requested.
		if ((this->m_projectName == "") || this->m_projectName == p.getName())
		{
			std::cout << p.toString();
		}
	}
}