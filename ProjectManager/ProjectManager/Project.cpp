#include "Project.h"

Project::Project(std::string m_name, std::string m_author)
{
	this->m_name = m_name;
	this->m_author = m_author;
}

Project::~Project()
{
	
}

std::string Project::getName()
{
	return this->m_name;
}


std::string Project::getAuthor()
{
	return this->m_author;
}

void Project::setName(std::string name)
{
	this->m_name = name;
}

void Project::setAuthor(std::string author)
{
	this->m_author = author;
}

std::string Project::toString()
{
	std::cout << "Name: " + this->getName + "\nAuthor: " + this->m_author + "\n";
}