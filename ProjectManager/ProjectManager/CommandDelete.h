#pragma once
#include "ICommand.h"

class CommandDelete : public ICommand
{
private:
	std::string m_projectName;

public:

	CommandDelete(std::string& projectName); // C'tor

	void doAction(std::vector<Project*>& projects) const;
};