#pragma once
#include <vector>
#include "Project.h"

class ICommand
{
public:
	void virtual doAction(std::vector<Project>& projects) = 0;
};